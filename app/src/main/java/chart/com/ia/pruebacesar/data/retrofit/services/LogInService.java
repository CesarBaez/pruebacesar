package chart.com.ia.pruebacesar.data.retrofit.services;

import chart.com.ia.pruebacesar.data.models.response.login.LogInResponse;
import chart.com.ia.pruebacesar.data.models.response.user.UserResponse;
import retrofit2.http.Field;
import retrofit2.http.Header;
import rx.Observable;

/**
 * Created by fbaez on 20/02/2018.
 */

public interface LogInService {
    Observable<LogInResponse> logIn(@Field("country_code") String country_code,
                                    @Field("username") String username,
                                    @Field("password") String password,
                                    @Field("grant_type") String grant_type,
                                    @Field("client_id") String client_id,
                                    @Field("client_secret") String client_secret);

    Observable<UserResponse> getUser(@Header("api_key") String api_key,
                                     @Header("Authorization") String Authorization);

}
