package chart.com.ia.pruebacesar.di.domain;

import android.content.Context;

import javax.inject.Singleton;

import chart.com.ia.pruebacesar.data.retrofit.services.BillBoardService;
import chart.com.ia.pruebacesar.data.retrofit.services.ComplejosService;
import chart.com.ia.pruebacesar.data.retrofit.services.LogInService;
import chart.com.ia.pruebacesar.data.retrofit.services.TransactionsService;
import chart.com.ia.pruebacesar.domain.BillBoardInteractor;
import chart.com.ia.pruebacesar.domain.ComplejoInteractor;
import chart.com.ia.pruebacesar.domain.LoginInteractor;
import chart.com.ia.pruebacesar.domain.TransactionsInteractor;
import dagger.Module;
import dagger.Provides;

/**
 * Created by fbaez on 20/02/2018.
 */
@Module
public class DomainModule {
    @Provides
    @Singleton
    LoginInteractor provideLoginInteractor(LogInService service, Context context) {
        return new LoginInteractor(context, service);
    }

    @Provides
    @Singleton
    TransactionsInteractor provideTransactionsInteractor(TransactionsService service, Context context) {
        return new TransactionsInteractor(service, context);
    }

    @Provides
    @Singleton
    ComplejoInteractor provideComplejoInteractor(ComplejosService service, Context context){
        return new ComplejoInteractor(service, context);
    }

    @Provides
    @Singleton
    BillBoardInteractor provideBillBoardInteractor(BillBoardService service,Context context){
        return  new BillBoardInteractor(context, service);
    }
}
