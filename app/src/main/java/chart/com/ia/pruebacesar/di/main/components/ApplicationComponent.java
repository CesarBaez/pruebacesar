package chart.com.ia.pruebacesar.di.main.components;

import javax.inject.Singleton;

import chart.com.ia.pruebacesar.di.data.DataModule;
import chart.com.ia.pruebacesar.di.domain.DomainModule;
import chart.com.ia.pruebacesar.di.main.modules.ApplicationModule;
import chart.com.ia.pruebacesar.ui.billboard.BillBoardFragment;
import chart.com.ia.pruebacesar.ui.billboard.MovieFragment;
import chart.com.ia.pruebacesar.ui.complejo.ComplejoFragment;
import chart.com.ia.pruebacesar.ui.home.HomeActivity;
import chart.com.ia.pruebacesar.ui.login.LoginActivity;
import chart.com.ia.pruebacesar.ui.profile.ClubCinepolisFragment;
import chart.com.ia.pruebacesar.ui.profile.ProfileFragment;
import dagger.Component;

/**
 * Created by fbaez on 20/02/2018.
 */

@Singleton
@Component(modules = {ApplicationModule.class, DataModule.class, DomainModule.class})
public interface ApplicationComponent {

    void inject(LoginActivity loginActivity);
    void inject(HomeActivity homeActivity);
    void inject(ProfileFragment profileFragment);
    void inject(ClubCinepolisFragment clubCinepolisFragment);
    void inject(BillBoardFragment billBoardFragment);
    void inject(ComplejoFragment complejoFragment);
    void inject(MovieFragment movieFragment);


}
