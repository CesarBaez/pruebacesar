package chart.com.ia.pruebacesar.domain;

import android.content.Context;

import chart.com.ia.pruebacesar.data.models.response.billboard.BillBoardResponse;
import chart.com.ia.pruebacesar.data.models.response.complejos.ComplejoItem;
import chart.com.ia.pruebacesar.data.retrofit.services.BillBoardService;
import rx.Subscription;

/**
 * Created by fbaez on 21/02/2018.
 */

public class BillBoardInteractor {
    private Context context;
    private Subscription subscription;
    private onBillBoardListener listener;
    private BillBoardService service;

    public BillBoardInteractor(Context context, BillBoardService service) {
        this.context = context;
        this.service = service;
    }


    public void setListener(onBillBoardListener listener) {
        this.listener = listener;
    }

    public void getBillBoard(String api_key) {
        subscription = service.getBillBoard(api_key).subscribe(billBoardResponse -> {
            if (listener != null) {
                listener.onDataSent(billBoardResponse);
            }
        }, throwable -> {
            if (listener != null) {
                listener.onServiceError(throwable.getMessage());
            }
        });

    }

    public interface onBillBoardListener {
        void onServiceError(String mensaje);

        void onDataSent(BillBoardResponse billBoardResponse);
    }

}
