package chart.com.ia.pruebacesar.data.models.request.login;

/**
 * Created by fbaez on 20/02/2018.
 */

public class LogInRequest {
    private String password;
    private String userName;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
