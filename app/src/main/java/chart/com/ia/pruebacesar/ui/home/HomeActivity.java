package chart.com.ia.pruebacesar.ui.home;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import java.util.HashMap;

import butterknife.BindView;
import chart.com.ia.pruebacesar.R;
import chart.com.ia.pruebacesar.ui.billboard.BillBoardFragment;
import chart.com.ia.pruebacesar.ui.complejo.ComplejoFragment;
import chart.com.ia.pruebacesar.ui.profile.ProfileFragment;
import ia.com.commons.view.BaseActivity;

public class HomeActivity extends BaseActivity {

    @BindView(R.id.home_navigation_view)
    BottomNavigationViewEx navigationView;

    @BindView(R.id.home_fragment_container)
    FrameLayout containerHome;

    private HashMap<Integer, Fragment> hashMapFragments = new HashMap<>();

    private Fragment fragment;

    private BottomNavigationViewEx.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationViewEx.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            fragment = hashMapFragments.get(item.getItemId());
            if (fragment != null) {
                clearFragmentStack();
                replaceFragment(fragment);
            }
            return true;
        }
    };

    private void addFragments() {
        hashMapFragments.put(R.id.navigation_profile, new ProfileFragment());
        hashMapFragments.put(R.id.navigation_billboard,new BillBoardFragment());
        hashMapFragments.put(R.id.navigation_complejo,new ComplejoFragment());
    }

    private void replaceFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .replace(containerHome.getId(), fragment)
                .commit();
    }

    public void clearFragmentStack() {
        FragmentManager fm = getSupportFragmentManager();
        for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_home;
    }


    @Override
    protected void initActivity() {
        super.initActivity();
        navigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigationView.enableAnimation(true);
        navigationView.enableShiftingMode(true);
        navigationView.enableItemShiftingMode(true);
        addFragments();
        replaceFragment(new ProfileFragment());

    }

    @Nullable
    @Override
    protected Toolbar getBaseToolbar() {
        return super.getBaseToolbar();
    }
}
