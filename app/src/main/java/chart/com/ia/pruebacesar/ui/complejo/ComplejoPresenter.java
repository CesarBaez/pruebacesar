package chart.com.ia.pruebacesar.ui.complejo;

import android.content.Context;

import com.google.gson.Gson;

import javax.inject.Inject;

import chart.com.ia.pruebacesar.data.local.PreferencesHelper;
import chart.com.ia.pruebacesar.data.models.response.complejos.ComplejoItem;
import chart.com.ia.pruebacesar.domain.ComplejoInteractor;
import ia.com.commons.view.BasePresenter;

/**
 * Created by fbaez on 21/02/2018.
 */

public class ComplejoPresenter extends BasePresenter<ComplejoPresenter.View> implements ComplejoInteractor.onComplejosListener {

    @Inject
    PreferencesHelper preferencesHelper;
    @Inject
    Context context;
    @Inject
    Gson gson;

    private ComplejoInteractor complejoInteractor;

    @Inject
    public ComplejoPresenter(ComplejoInteractor complejoInteractor) {
        this.complejoInteractor = complejoInteractor;
        this.complejoInteractor.setListener(this);
    }

    public void getComplejo(String api_key) {
        complejoInteractor.getComplejo(api_key);
        if (isViewAttached()) {
            getView().showLoading();
        }
    }

    @Override
    public void onServiceError(String mensaje) {
        if (isViewAttached()) {
            getView().onComplejoServiceError(mensaje);
            getView().hideLoading();
        }
    }

    @Override
    public void onDataSent(ComplejoItem complejoItem) {
        if (isViewAttached()) {
            preferencesHelper.setString(PreferencesHelper.COMPLEJOS_OBJECT, gson.toJson(complejoItem));
            getView().onComplejoServiceSuccess(complejoItem);
            getView().hideLoading();
        }

    }

    public interface View extends BasePresenter.View {
        void onComplejoServiceSuccess(ComplejoItem complejoItem);

        void onComplejoServiceError(String error);

        @Override
        void showLoading();

        @Override
        void hideLoading();
    }
}
