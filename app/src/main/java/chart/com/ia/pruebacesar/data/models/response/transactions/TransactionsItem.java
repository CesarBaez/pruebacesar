package chart.com.ia.pruebacesar.data.models.response.transactions;


import com.google.gson.annotations.SerializedName;


public class TransactionsItem{

	@SerializedName("date")
	private String date;

	@SerializedName("cinema")
	private String cinema;

	@SerializedName("message")
	private String message;

	@SerializedName("points")
	private Double points;

	public void setDate(String date){
		this.date = date;
	}

	public String getDate(){
		return date;
	}

	public void setCinema(String cinema){
		this.cinema = cinema;
	}

	public String getCinema(){
		return cinema;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setPoints(Double points){
		this.points = points;
	}

	public Double getPoints(){
		return points;
	}
}