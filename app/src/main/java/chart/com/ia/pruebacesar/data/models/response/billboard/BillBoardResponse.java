package chart.com.ia.pruebacesar.data.models.response.billboard;

import java.util.List;
import com.google.gson.annotations.SerializedName;


public class BillBoardResponse{

	@SerializedName("movies")
	private List<MoviesItem> movies;

	@SerializedName("routes")
	private List<RoutesItem> routes;

	public void setMovies(List<MoviesItem> movies){
		this.movies = movies;
	}

	public List<MoviesItem> getMovies(){
		return movies;
	}

	public void setRoutes(List<RoutesItem> routes){
		this.routes = routes;
	}

	public List<RoutesItem> getRoutes(){
		return routes;
	}
}