package chart.com.ia.pruebacesar.data;

/**
 * Created by fbaez on 20/02/2018.
 */

public class DataConfiguration {
    public static final String BASE_URL = "https://api-stage.cinepolis.com/";

    private String baseUrl;

    public DataConfiguration(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public static class LoginApi {
        public static final String LOGIN = "v2/oauth/token";
        public static final String GETUSER = "v1/members/profile?country_code=MX";
    }
    public static class Transactions{
        public static final String GET_TRANSACTIONS="v1/members/loyalty/";
    }

    public static class BillBoard{
        public static  final String GET_BILLBOARD="v2/movies?country_code=MX&cinemas=32";
    }
    public static class Complejos{
        public static  final String GET_COMPLEJO="v2/locations/cinemas/69?country_code=MX";
    }
}
