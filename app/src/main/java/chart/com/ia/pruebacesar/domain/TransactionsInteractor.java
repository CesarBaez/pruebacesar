package chart.com.ia.pruebacesar.domain;

import android.content.Context;

import chart.com.ia.pruebacesar.data.models.request.transactions.TransactionsRequest;
import chart.com.ia.pruebacesar.data.models.response.transactions.TransactionsResponse;
import chart.com.ia.pruebacesar.data.retrofit.services.TransactionsService;
import rx.Subscription;

/**
 * Created by fbaez on 20/02/2018.
 */

public class TransactionsInteractor {

    TransactionsService service;
    Context context;
    OnTransactionsListener listener;
    private Subscription subscription;


    public TransactionsInteractor(TransactionsService service, Context context) {
        this.service = service;
        this.context = context;
    }

    public void getTransactions(String authorization, TransactionsRequest transactionsRequest) {
        subscription = service.getTransactions(authorization, transactionsRequest).subscribe(transactionsResponse -> {
            if (listener != null) {
                listener.onDataSent(transactionsResponse);
            }
        }, throwable -> {
            if (listener != null) {
                listener.onServiceError(throwable.getMessage());
            }
        });
    }

    public void setListener(OnTransactionsListener listener) {
        this.listener = listener;
    }

    public interface OnTransactionsListener {

        void onServiceError(String mensaje);

        void onDataSent(TransactionsResponse transactionsResponse);

    }
}
