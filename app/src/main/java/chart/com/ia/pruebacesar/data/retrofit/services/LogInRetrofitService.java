package chart.com.ia.pruebacesar.data.retrofit.services;

import chart.com.ia.pruebacesar.data.DataConfiguration;
import chart.com.ia.pruebacesar.data.models.response.login.LogInResponse;
import chart.com.ia.pruebacesar.data.models.response.user.UserResponse;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by fbaez on 20/02/2018.
 */

public interface LogInRetrofitService {

    @FormUrlEncoded
    @Headers({
            "api_key:199e2ce46ac525fddf",
            "Content-Type:application/x-www-form-urlencoded"
    })
    @POST(DataConfiguration.LoginApi.LOGIN)
    Observable<LogInResponse> logIn(
            @Field("country_code") String country_code,
            @Field("username") String username,
            @Field("password") String password,
            @Field("grant_type") String grant_type,
            @Field("client_id") String client_id,
            @Field("client_secret") String client_secret
            );

    @GET(DataConfiguration.LoginApi.GETUSER)
    Observable<UserResponse> getUser(
      @Header("api_key") String api_key,
      @Header("Authorization") String Authorization
    );
}
