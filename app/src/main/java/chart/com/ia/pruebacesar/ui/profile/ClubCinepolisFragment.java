package chart.com.ia.pruebacesar.ui.profile;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import chart.com.ia.pruebacesar.App;
import chart.com.ia.pruebacesar.R;
import chart.com.ia.pruebacesar.Utils;
import chart.com.ia.pruebacesar.data.local.PreferencesHelper;
import chart.com.ia.pruebacesar.data.models.response.login.LogInResponse;
import chart.com.ia.pruebacesar.data.models.response.transactions.BalanceListItem;
import chart.com.ia.pruebacesar.data.models.response.transactions.TransactionsResponse;
import chart.com.ia.pruebacesar.data.models.response.user.UserResponse;
import chart.com.ia.pruebacesar.ui.adapters.TransactionsAdapter;
import chart.com.ia.pruebacesar.ui.login.LoginActivity;
import ia.com.commons.view.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class ClubCinepolisFragment extends BaseFragment implements ClubCinepolisPresenter.View {
    @Inject
    ClubCinepolisPresenter presenter;
    @Inject
    Context context;
    @Inject
    Gson gson;
    @Inject
    PreferencesHelper preferencesHelper;
    @BindView(R.id.tv_club_nombre)
    TextView tvClubNombre;
    @BindView(R.id.tv_club_email)
    TextView tvClubEmail;
    @BindView(R.id.tv_club_card_number)
    TextView tvClubCardNumber;
    @BindView(R.id.tv_club_puntos)
    TextView tvClubPuntos;
    @BindView(R.id.tv_club_message_next_level)
    TextView tvClubMessageNextLevel;
    @BindView(R.id.tv_club_level)
    TextView tvClubLevel;
    @BindView(R.id.chart_club_advance_level)
    PieChart pieChartAdvanceLevel;
    @BindView(R.id.recycler_transactions_club)
    RecyclerView rvTransactions;

    private TransactionsResponse transactionsObj;
    private ProgressDialog progressDialog;


    final public static String CARD_NUMBER_ARG = "card_number";
    private String cardNumber;
    private LogInResponse tokenObj;

    @OnClick(R.id.logout)
    public void onClickLogOut() {
        showAlertDialog();
    }

    public ClubCinepolisFragment() {
        // Required empty public constructor
    }

    @Override
    protected void initFragment(@NonNull View view) {
        super.initFragment(view);
        initDagger();
        hideLoading();
        tokenObj = gson.fromJson(preferencesHelper.getString(PreferencesHelper.TOKEN_OBJECT, ""), LogInResponse.class);
        if (getArguments() != null) {
            cardNumber = getArguments().getString(CARD_NUMBER_ARG);
        }

    }

    @Override
    protected void initPresenter() {
        super.initPresenter();
        presenter.setView(this);
        if (Utils.isMobileNetConected(context) || Utils.isWifiConected(context)) {
            presenter.getTransactions(tokenObj.getTokenType() + " " + tokenObj.getAccessToken(), cardNumber);
        } else {
            Snackbar.make(getView(), getString(R.string.message_no_conexion), Snackbar.LENGTH_LONG).show();
            initWithLocalData();
        }


    }

    private void setViewsData() {
        tvClubNombre.setText(transactionsObj.getName());
        tvClubEmail.setText(transactionsObj.getEmail());
        tvClubCardNumber.setText(cardNumber);
        List<BalanceListItem> balanceList = transactionsObj.getBalanceList();
        for (int i = 0; i < balanceList.size(); i++) {
            if (balanceList.get(i).getKey().equals("points")) {
                tvClubPuntos.setText("" + balanceList.get(i).getBalance());
            }
            if (balanceList.get(i).getKey().equals("visits")) {
                initPieChart(transactionsObj.getLevel().getAdvancePercent(), (int) balanceList.get(i).getBalance(), balanceList.get(i).getMessage());
            }

        }
        tvClubLevel.setText(transactionsObj.getLevel().getName());
        tvClubMessageNextLevel.setText(transactionsObj.getLevel().getMessage());
        initRecyclerView();
    }

    public void initRecyclerView() {
        TransactionsAdapter adapter = new TransactionsAdapter(transactionsObj.getTransactions(), context);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rvTransactions.setLayoutManager(layoutManager);
        rvTransactions.setAdapter(adapter);
        hideLoading();
    }

    private void initDagger() {
        App.getApplicationComponents().inject(this);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_club_cinepolis;
    }


    private void initPieChart(Double porcent, int value, String maxValue) {
        List<PieEntry> entries = new ArrayList<>();

        entries.add(new PieEntry((int) (porcent * 100), ""));
        entries.add(new PieEntry(100 - ((int) (porcent * 100)), ""));

        PieDataSet set = new PieDataSet(entries, "");

        set.setColors(new int[]{R.color.color_accent, R.color.grey_with_transparnce}, getActivity());
        PieData data = new PieData(set);
        data.setDrawValues(false);
        pieChartAdvanceLevel.setData(data);
        pieChartAdvanceLevel.invalidate(); // refresh
        pieChartAdvanceLevel.setRotationEnabled(false);
        // pieChart.setDrawMarkers(false);
        Description description = new Description();
        description.setText("");
        pieChartAdvanceLevel.setDescription(description);
        Legend l = pieChartAdvanceLevel.getLegend();
        l.setEnabled(false);
        pieChartAdvanceLevel.setHighlightPerTapEnabled(false);
        pieChartAdvanceLevel.setCenterText(generateCenterSpannableText(value, maxValue));

    }

    private SpannableString generateCenterSpannableText(int value, String maxValue) {

        SpannableString s = new SpannableString(value + " " + maxValue);
        s.setSpan(new RelativeSizeSpan(1.1f), 0, s.length(), 0);
        // s.setSpan(new StyleSpan(Typeface.NORMAL), s.length()-1, 0, 0);
        s.setSpan(new ForegroundColorSpan(Color.DKGRAY), 0, s.length(), 0);
        return s;
    }

    @Override
    public void onTransactionsServiceSuccess(TransactionsResponse transactionsResponse) {
        transactionsObj = transactionsResponse;
        setViewsData();
    }

    @Override
    public void onTransactionServiceError(String error) {
        Snackbar.make(getView(), error, Snackbar.LENGTH_LONG).show();
        initWithLocalData();
    }

    @Override
    public void showLoading() {
        progressDialog = ProgressDialog.show(getActivity(), null, getString(R.string.loading_data_text_dialog), true);
    }

    @Override
    public void hideLoading() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    private void initWithLocalData() {
        if (!preferencesHelper.getString(PreferencesHelper.TRANSACTIONS_OBJECT, "").isEmpty()) {
            transactionsObj = gson.fromJson(preferencesHelper.getString(PreferencesHelper.TRANSACTIONS_OBJECT, ""), TransactionsResponse.class);
            setViewsData();
        }
    }

    //Method to close the actual sesion
    private void showAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                preferencesHelper.clearInfoByLogout();
                startActivity(new Intent(getActivity(), LoginActivity.class));
                getActivity().finish();
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });
        builder.setMessage(R.string.dialog_message)
                .setTitle(R.string.dialog_title);
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}

