package chart.com.ia.pruebacesar.data.models.response.transactions;

import java.util.List;
import com.google.gson.annotations.SerializedName;


public class TransactionsResponse{

	@SerializedName("balance_list")
	private List<BalanceListItem> balanceList;

	@SerializedName("level")
	private Level level;

	@SerializedName("name")
	private String name;

	@SerializedName("transactions")
	private List<TransactionsItem> transactions;

	@SerializedName("email")
	private String email;

	public void setBalanceList(List<BalanceListItem> balanceList){
		this.balanceList = balanceList;
	}

	public List<BalanceListItem> getBalanceList(){
		return balanceList;
	}

	public void setLevel(Level level){
		this.level = level;
	}

	public Level getLevel(){
		return level;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setTransactions(List<TransactionsItem> transactions){
		this.transactions = transactions;
	}

	public List<TransactionsItem> getTransactions(){
		return transactions;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}
}