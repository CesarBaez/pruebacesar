package chart.com.ia.pruebacesar.data.models.response.login;


import com.google.gson.annotations.SerializedName;

public class LogInResponse{

	@SerializedName("access_token")
	private String accessToken;

	@SerializedName("refresh_token")
	private String refreshToken;

	@SerializedName("country_code")
	private String countryCode;

	@SerializedName("as:client_id")
	private String asClientId;

	@SerializedName(".expires")
	private String expires;

	@SerializedName("token_type")
	private String tokenType;

	@SerializedName("expires_in")
	private int expiresIn;

	@SerializedName("username")
	private String username;

	@SerializedName(".issued")
	private String issued;

	public void setAccessToken(String accessToken){
		this.accessToken = accessToken;
	}

	public String getAccessToken(){
		return accessToken;
	}

	public void setRefreshToken(String refreshToken){
		this.refreshToken = refreshToken;
	}

	public String getRefreshToken(){
		return refreshToken;
	}

	public void setCountryCode(String countryCode){
		this.countryCode = countryCode;
	}

	public String getCountryCode(){
		return countryCode;
	}

	public void setAsClientId(String asClientId){
		this.asClientId = asClientId;
	}

	public String getAsClientId(){
		return asClientId;
	}

	public void setExpires(String expires){
		this.expires = expires;
	}

	public String getExpires(){
		return expires;
	}

	public void setTokenType(String tokenType){
		this.tokenType = tokenType;
	}

	public String getTokenType(){
		return tokenType;
	}

	public void setExpiresIn(int expiresIn){
		this.expiresIn = expiresIn;
	}

	public int getExpiresIn(){
		return expiresIn;
	}

	public void setUsername(String username){
		this.username = username;
	}

	public String getUsername(){
		return username;
	}

	public void setIssued(String issued){
		this.issued = issued;
	}

	public String getIssued(){
		return issued;
	}
}