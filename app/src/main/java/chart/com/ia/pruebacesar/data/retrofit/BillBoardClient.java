package chart.com.ia.pruebacesar.data.retrofit;

import chart.com.ia.pruebacesar.data.models.response.billboard.BillBoardResponse;
import chart.com.ia.pruebacesar.data.retrofit.services.BillBoardRetrofitService;
import chart.com.ia.pruebacesar.data.retrofit.services.BillBoardService;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by fbaez on 21/02/2018.
 */

public class BillBoardClient implements BillBoardService {

    private BillBoardRetrofitService service;

    public BillBoardClient(BillBoardRetrofitService service) {
        this.service = service;
    }

    @Override
    public Observable<BillBoardResponse> getBillBoard(String api_key) {
        return service.getBillBoard(api_key)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
