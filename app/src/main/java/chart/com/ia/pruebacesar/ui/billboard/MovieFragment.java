package chart.com.ia.pruebacesar.ui.billboard;


import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareButton;
import com.facebook.share.widget.ShareDialog;
import com.github.rtoshiro.view.video.FullscreenVideoLayout;
import com.google.gson.Gson;

import java.io.IOException;

import javax.inject.Inject;

import butterknife.BindView;
import chart.com.ia.pruebacesar.App;
import chart.com.ia.pruebacesar.R;
import chart.com.ia.pruebacesar.Utils;
import chart.com.ia.pruebacesar.data.local.PreferencesHelper;
import chart.com.ia.pruebacesar.data.models.response.billboard.MoviesItem;
import ia.com.commons.view.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class MovieFragment extends BaseFragment {
    public static final String MOVIE_OBJECT = "data_movie";
    public static final String BASE_VIDEO_URL = "base_video_url";

    private String baseVideoUrl;
    @Inject
    Context context;
    @Inject
    Gson gson;
    @Inject
    PreferencesHelper preferencesHelper;

    private MoviesItem moviesItem;
    @BindView(R.id.videoview)
    FullscreenVideoLayout videoLayout;
    @BindView(R.id.tv_movie_name)
    TextView tvMovieName;
    @BindView(R.id.tv_movie_gender)
    TextView tvMovieGender;
    @BindView(R.id.tv_movie_duration)
    TextView tvMovieDuration;
    @BindView(R.id.tv_movie_information)
    TextView tvMovieSinopsis;

    @BindView(R.id.facebook_share_button)
    ShareButton shareButton;



    public MovieFragment() {
        // Required empty public constructor
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_movie;
    }

    private void initDagger() {
        App.getApplicationComponents().inject(this);
    }

    @Override
    protected void initFragment(@NonNull View view) {
        super.initFragment(view);
        initDagger();
        if (getArguments() != null) {
            moviesItem = gson.fromJson(getArguments().getString(MOVIE_OBJECT), MoviesItem.class);
            baseVideoUrl = getArguments().getString(BASE_VIDEO_URL);
        }
        initDataViews();

        videoLayout.setActivity(getActivity());
    }

    private void initDataViews() {
        tvMovieName.setText(moviesItem.getName());
        tvMovieGender.setText(moviesItem.getGenre());
        tvMovieDuration.setText(moviesItem.getLength());
        tvMovieSinopsis.setText(moviesItem.getSynopsis());
        for (int i = 0; i < moviesItem.getMedia().size(); i++) {
            if (moviesItem.getMedia().get(i).getCode().equals("trailer_mp4")) {
                setVideoLayout(baseVideoUrl + moviesItem.getMedia().get(i).getResource());
                shareButton.setShareContent(Utils.getSharedLinkContent(baseVideoUrl + moviesItem.getMedia().get(i).getResource(), moviesItem.getName()));
                break;
            }
        }


    }

    private void setVideoLayout(String url) {
        if (Utils.isWifiConected(context)||Utils.isMobileNetConected(context)){
            Uri videoUri = Uri.parse(url);
            try {
                videoLayout.setVideoURI(videoUri);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
