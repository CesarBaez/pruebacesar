package chart.com.ia.pruebacesar.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import chart.com.ia.pruebacesar.R;
import chart.com.ia.pruebacesar.Utils;
import chart.com.ia.pruebacesar.data.models.response.billboard.MediaItem;
import chart.com.ia.pruebacesar.data.models.response.billboard.MoviesItem;
import okhttp3.internal.Util;

/**
 * Created by fbaez on 21/02/2018.
 */

public class BillBoardAdapter extends RecyclerView.Adapter<BillBoardAdapter.ViewHolder> {

    private Context context;
    private List<MoviesItem> movies;
    private onMovieClickListener listener;
    private String baseImageUrl;

    public BillBoardAdapter(Context context, List<MoviesItem> movies, onMovieClickListener listener,String baseImageUrl) {
        this.context = context;
        this.movies = movies;
        this.listener = listener;
        this.baseImageUrl=baseImageUrl;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.cartel_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        MoviesItem item = movies.get(position);
        holder.tvTitle.setText(item.getName());
        holder.tvGender.setText(item.getGenre());
        holder.tvInformation.setText(item.getSynopsis());
        for (int i=0;i<item.getMedia().size();i++){
           if (item.getMedia().get(i).getCode().equals("poster")){
               Utils.putImageOnView(holder.ivCartel,baseImageUrl+item.getMedia().get(i).getResource(),context);
               break;
           }
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            listener.onMovieClicked(item);
            }
        });
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle, tvInformation, tvGender;
        ImageView ivCartel;

        public ViewHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_movie_title);
            tvInformation = (TextView) itemView.findViewById(R.id.tv_movie_information);
            tvGender = (TextView) itemView.findViewById(R.id.tv_movie_gender);
            ivCartel = (ImageView) itemView.findViewById(R.id.imagen_cartel);
        }
    }

    public interface onMovieClickListener {
        void onMovieClicked(MoviesItem moviesItem);
    }
}
