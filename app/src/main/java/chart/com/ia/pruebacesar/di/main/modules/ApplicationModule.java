package chart.com.ia.pruebacesar.di.main.modules;

import android.app.Application;
import android.content.Context;

import com.google.gson.Gson;

import javax.inject.Singleton;

import chart.com.ia.pruebacesar.App;
import chart.com.ia.pruebacesar.data.local.PreferencesHelper;
import dagger.Module;
import dagger.Provides;

/**
 * Created by fbaez on 20/02/2018.
 */
@Module
public class ApplicationModule {

    private final App application;

    public ApplicationModule(App application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Application providesApplication() {
        return application;
    }

    @Provides
    @Singleton
    Context providesContext() {
        return application.getApplicationContext();
    }

    @Provides
    @Singleton
    PreferencesHelper providesPreferencesHelper(Context context) {
        return new PreferencesHelper(context);
    }

    @Provides
    @Singleton
    Gson providesGson() {
        return new Gson();
    }
}
