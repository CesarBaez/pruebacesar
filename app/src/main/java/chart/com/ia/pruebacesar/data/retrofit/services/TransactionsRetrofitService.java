package chart.com.ia.pruebacesar.data.retrofit.services;

import chart.com.ia.pruebacesar.data.DataConfiguration;
import chart.com.ia.pruebacesar.data.models.request.transactions.TransactionsRequest;
import chart.com.ia.pruebacesar.data.models.response.login.LogInResponse;
import chart.com.ia.pruebacesar.data.models.response.transactions.TransactionsResponse;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by fbaez on 20/02/2018.
 */

public interface TransactionsRetrofitService {

    @Headers({
            "api_key:199e2ce46ac525fddf",
            "Content-Type:application/json"
    })
    @POST(DataConfiguration.Transactions.GET_TRANSACTIONS)
    Observable<TransactionsResponse> getTransactions(
            @Header("Authorization") String authorization,
            @Body TransactionsRequest transactionsRequest
            );
}
