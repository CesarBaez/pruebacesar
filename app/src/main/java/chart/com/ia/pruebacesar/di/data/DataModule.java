package chart.com.ia.pruebacesar.di.data;

import com.google.gson.Gson;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import chart.com.ia.pruebacesar.data.DataConfiguration;
import chart.com.ia.pruebacesar.data.retrofit.BillBoardClient;
import chart.com.ia.pruebacesar.data.retrofit.ComplejosClient;
import chart.com.ia.pruebacesar.data.retrofit.LogInClient;
import chart.com.ia.pruebacesar.data.retrofit.TransactionsClient;
import chart.com.ia.pruebacesar.data.retrofit.services.BillBoardRetrofitService;
import chart.com.ia.pruebacesar.data.retrofit.services.BillBoardService;
import chart.com.ia.pruebacesar.data.retrofit.services.ComplejosRetrofitService;
import chart.com.ia.pruebacesar.data.retrofit.services.ComplejosService;
import chart.com.ia.pruebacesar.data.retrofit.services.LogInRetrofitService;
import chart.com.ia.pruebacesar.data.retrofit.services.LogInService;
import chart.com.ia.pruebacesar.data.retrofit.services.TransactionsRetrofitService;
import chart.com.ia.pruebacesar.data.retrofit.services.TransactionsService;
import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.schedulers.Schedulers;

/**
 * Created by fbaez on 20/02/2018.
 */
@Module
public class DataModule {

    private String baseUrl;

    public DataModule(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    @Provides
    @Singleton
    DataConfiguration providesDataConfiguration() {
        return new DataConfiguration(baseUrl);
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient() {
        return new OkHttpClient.Builder()
                .readTimeout(180, TimeUnit.SECONDS)
                .connectTimeout(180, TimeUnit.SECONDS)
                .build();
    }

    @Provides
    @Singleton
    Retrofit providesRetrofit(DataConfiguration dataConfiguration, OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(dataConfiguration.getBaseUrl())
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io()))
                .client(okHttpClient)
                .build();
    }

    @Provides
    @Singleton
    LogInService provideLogInService(Retrofit retrofit) {
        return new LogInClient(retrofit.create(LogInRetrofitService.class));
    }

    @Provides
    @Singleton
    TransactionsService provideTransactionsService(Retrofit retrofit) {
        return new TransactionsClient(retrofit.create(TransactionsRetrofitService.class));
    }

    @Provides
    @Singleton
    BillBoardService provideBillBoardService(Retrofit retrofit) {
        return new BillBoardClient(retrofit.create(BillBoardRetrofitService.class));
    }

    @Provides
    @Singleton
    ComplejosService provideComplejosService(Retrofit retrofit){
        return new ComplejosClient(retrofit.create(ComplejosRetrofitService.class));
    }
}
