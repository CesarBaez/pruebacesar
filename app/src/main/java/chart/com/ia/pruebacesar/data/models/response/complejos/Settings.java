package chart.com.ia.pruebacesar.data.models.response.complejos;


import com.google.gson.annotations.SerializedName;


public class Settings{

	@SerializedName("cs_merchant_id")
	private String csMerchantId;

	@SerializedName("type_food_sales")
	private String typeFoodSales;

	@SerializedName("is_special_prices")
	private boolean isSpecialPrices;

	@SerializedName("vco_merchant_id")
	private String vcoMerchantId;

	public void setCsMerchantId(String csMerchantId){
		this.csMerchantId = csMerchantId;
	}

	public String getCsMerchantId(){
		return csMerchantId;
	}

	public void setTypeFoodSales(String typeFoodSales){
		this.typeFoodSales = typeFoodSales;
	}

	public String getTypeFoodSales(){
		return typeFoodSales;
	}

	public void setIsSpecialPrices(boolean isSpecialPrices){
		this.isSpecialPrices = isSpecialPrices;
	}

	public boolean isIsSpecialPrices(){
		return isSpecialPrices;
	}

	public void setVcoMerchantId(String vcoMerchantId){
		this.vcoMerchantId = vcoMerchantId;
	}

	public String getVcoMerchantId(){
		return vcoMerchantId;
	}
}