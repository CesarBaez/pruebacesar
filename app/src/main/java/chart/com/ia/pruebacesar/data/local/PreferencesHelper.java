package chart.com.ia.pruebacesar.data.local;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by fbaez on 20/02/2018.
 */

public class PreferencesHelper {

    private final SharedPreferences preferences;
    private final SharedPreferences.Editor editor;
    private static final String PREFERENCES_NAME = "prueva.cesar.preferences";
    public static final String LOGGED = "logged";
    public static final String TOKEN_OBJECT = "token_object";
    public static final String USER_OBJECT = "user_object";
    public static final String BILL_BOARD_OBJECT = "bill_board_object";
    public static final String COMPLEJOS_OBJECT = "complejos_object";
    public static final String TRANSACTIONS_OBJECT = "transactions_object";

    public PreferencesHelper(Context context) {
        this.preferences = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
        this.editor = preferences.edit();

    }


    /**
     * Obtains a boolean from the application preferences
     *
     * @param preference Preference key
     * @param defValue   Default value if the key does not exists
     * @return the current preference
     */
    public boolean getBoolean(String preference, boolean defValue) {
        return preferences.getBoolean(preference, defValue);
    }

    /**
     * Storage a boolean in the application preferences
     *
     * @param preference
     * @param newValue
     */
    public void setBoolean(String preference, boolean newValue) {
        editor.putBoolean(preference, newValue);
        editor.commit();
    }

    /**
     * Obtains a String from the application preferences
     *
     * @param preference Preference key
     * @param defValue   Default value if the key does not exists
     * @return the current preference
     */
    public String getString(String preference, String defValue) {
        return preferences.getString(preference, defValue);
    }

    /**
     * Obtains a int from the application preferences
     *
     * @param preference Preference key
     * @param defValue   Default value if the key does not exists
     * @return the current preference
     */
    public int getInt(String preference, int defValue) {
        return preferences.getInt(preference, defValue);
    }

    /**
     * Storage a String in the application preferences
     *
     * @param preference
     * @param newValue
     */
    public void setString(String preference, String newValue) {
        editor.putString(preference, newValue);
        editor.commit();
    }

    /**
     * Storage a Int in the application preferences
     *
     * @param preference
     * @param newValue
     */
    public void setInt(String preference, int newValue) {
        editor.putInt(preference, newValue);
        editor.commit();
    }

    public void clearInfoByLogout() {
        editor.remove(LOGGED);
        editor.remove(TOKEN_OBJECT);
        editor.remove(USER_OBJECT);
        editor.remove(BILL_BOARD_OBJECT);
        editor.remove(COMPLEJOS_OBJECT);
        editor.remove(TRANSACTIONS_OBJECT);
        editor.commit();
    }
}
