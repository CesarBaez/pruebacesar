package chart.com.ia.pruebacesar.ui.billboard;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;

import javax.inject.Inject;

import butterknife.BindView;
import chart.com.ia.pruebacesar.App;
import chart.com.ia.pruebacesar.R;
import chart.com.ia.pruebacesar.Utils;
import chart.com.ia.pruebacesar.data.local.PreferencesHelper;
import chart.com.ia.pruebacesar.data.models.response.billboard.BillBoardResponse;
import chart.com.ia.pruebacesar.data.models.response.billboard.MoviesItem;
import chart.com.ia.pruebacesar.ui.adapters.BillBoardAdapter;
import chart.com.ia.pruebacesar.ui.adapters.TransactionsAdapter;
import chart.com.ia.pruebacesar.ui.profile.ClubCinepolisFragment;
import ia.com.commons.view.BaseFragment;


public class BillBoardFragment extends BaseFragment implements BillBoardPresenter.View, BillBoardAdapter.onMovieClickListener {

    @BindView(R.id.recycler_billboard)
    RecyclerView rvBillBoard;
    @Inject
    Context context;
    @Inject
    Gson gson;
    @Inject
    PreferencesHelper preferencesHelper;
    @Inject
    BillBoardPresenter presenter;

    private BillBoardResponse billBoardObj;

    private ProgressDialog progressDialog;

    public BillBoardFragment() {
        // Required empty public constructor
    }

    @Override
    protected void initFragment(@NonNull View view) {
        super.initFragment(view);
        initDagger();
        hideLoading();
    }

    @Override
    protected void initPresenter() {
        super.initPresenter();
        presenter.setView(this);
        if (Utils.isMobileNetConected(context) || Utils.isWifiConected(context)) {
            presenter.getBillBoard("199e2ce46ac525fddf");
        } else {
            Snackbar.make(getView(), getString(R.string.message_no_conexion), Snackbar.LENGTH_LONG).show();
            //Utils.showAlertDialog(getString(R.string.message_no_conexion), getString(R.string.alert_title), getActivity());
            initWithLocalData();
        }
    }

    public void initRecyclerView() {
        BillBoardAdapter adapter = new BillBoardAdapter(context, billBoardObj.getMovies(), this, obtenerBaseURl("poster"));
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rvBillBoard.setLayoutManager(layoutManager);
        rvBillBoard.setAdapter(adapter);
    }

    private String obtenerBaseURl(String key) {
        for (int i = 0; i < billBoardObj.getRoutes().size(); i++) {
            if (billBoardObj.getRoutes().get(i).getCode().equals(key)) {
                if (billBoardObj.getRoutes().get(i).getSizes().getSmall() != null) {
                    return billBoardObj.getRoutes().get(i).getSizes().getSmall();
                } else {
                    return billBoardObj.getRoutes().get(i).getSizes().getMedium();
                }

            }
        }
        return "";
    }

    private void initDagger() {
        App.getApplicationComponents().inject(this);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_bill_board;
    }


    @Override
    public void onBillBoardServiceSuccess(BillBoardResponse billBoardResponse) {
        billBoardObj = billBoardResponse;
        initRecyclerView();
    }

    @Override
    public void onBillBoardServiceError(String error) {
        Snackbar.make(getView(), getString(R.string.offline_status) + "\n" + error, Snackbar.LENGTH_LONG).show();
        initWithLocalData();
    }

    @Override
    public void showLoading() {
        progressDialog = ProgressDialog.show(getActivity(), null, getString(R.string.loading_data_text_dialog), true);
    }

    @Override
    public void hideLoading() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void onMovieClicked(MoviesItem moviesItem) {
        Bundle bundle = new Bundle();
        bundle.putString(MovieFragment.MOVIE_OBJECT, gson.toJson(moviesItem));
        bundle.putString(MovieFragment.BASE_VIDEO_URL, obtenerBaseURl("trailer_mp4"));
        Fragment movieFragment = new MovieFragment();
        movieFragment.setArguments(bundle);
        onReplaceTransactionWithBackStack(R.id.home_fragment_container, movieFragment);
    }

    private void initWithLocalData(){
        if (!preferencesHelper.getString(PreferencesHelper.BILL_BOARD_OBJECT, "").isEmpty()) {
            billBoardObj = gson.fromJson(preferencesHelper.getString(PreferencesHelper.BILL_BOARD_OBJECT, ""), BillBoardResponse.class);
            initRecyclerView();
        }
    }
}
