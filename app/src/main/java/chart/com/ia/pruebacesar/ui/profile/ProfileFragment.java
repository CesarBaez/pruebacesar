package chart.com.ia.pruebacesar.ui.profile;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import chart.com.ia.pruebacesar.App;
import chart.com.ia.pruebacesar.R;
import chart.com.ia.pruebacesar.Utils;
import chart.com.ia.pruebacesar.data.local.PreferencesHelper;
import chart.com.ia.pruebacesar.data.models.response.user.UserResponse;
import chart.com.ia.pruebacesar.ui.login.LoginActivity;
import ia.com.commons.view.BaseFragment;


public class ProfileFragment extends BaseFragment {


    @BindView(R.id.container_club_cinepolis)
    LinearLayout layoutClubCinepolis;
    @BindView(R.id.profile_text_tv_name)
    TextView tvName;
    @BindView(R.id.profile_text_tv_email)
    TextView tvEmail;
    @BindView(R.id.profile_text_tv_phone)
    TextView tvPhone;
    @BindView(R.id.profile_iv)
    ImageView ivImageProfile;

    @BindView(R.id.et_card_number)
    EditText etCardNumber;

    @Inject
    PreferencesHelper preferencesHelper;
    @Inject
    Gson gson;

    private UserResponse userObj;



    @OnClick(R.id.logout)
    public void onClickLogOut() {
        showAlertDialog();
    }


    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_profile;
    }

    @Override
    protected void initFragment(@NonNull View view) {
        super.initFragment(view);
        initDagger();
        verifyClubCard();
        setDataViews();
        setCardTextWacher();
        view.clearFocus();
    }

    private void setCardTextWacher() {
        etCardNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length()==16){
                    replaceCardNumber(s.toString());
                    showTransactions();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void verifyClubCard(){
        String userData = preferencesHelper.getString(PreferencesHelper.USER_OBJECT, "");
        if (!userData.isEmpty()) {
            userObj = gson.fromJson(userData, UserResponse.class);
            if (userObj.getCardNumber().isEmpty()) {
                Utils.showAlertDialog(getString(R.string.no_card_message), getString(R.string.alert_title), getActivity());
            } else {
                showTransactions();
            }

        } else {
            preferencesHelper.clearInfoByLogout();
            startActivity(new Intent(getActivity(), LoginActivity.class));
        }
    }


    private void initDagger() {
        App.getApplicationComponents().inject(this);
    }

    private void setDataViews() {
        tvName.setText(userObj.getName());
        tvEmail.setText(userObj.getEmail());
        tvPhone.setText(userObj.getPhoneNumber());
        if (!userObj.getProfilePicture().isEmpty()) {
            Utils.putImageOnView(ivImageProfile, userObj.getProfilePicture(), getContext());
        }


    }
    private void showTransactions(){
        Bundle bundleCollaborator = new Bundle();
        bundleCollaborator.putString(ClubCinepolisFragment.CARD_NUMBER_ARG, userObj.getCardNumber());
        Fragment clubCinepolisFragment = new ClubCinepolisFragment();
        clubCinepolisFragment.setArguments(bundleCollaborator);
        onReplaceTransaction(R.id.home_fragment_container, clubCinepolisFragment);

    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

    //Method to validate the imput of card number
    public boolean validImput() {
        boolean valid = true;
        if (etCardNumber.getText().toString().isEmpty() || etCardNumber.getText().toString().length() < 16) {
            etCardNumber.setError(getContext().getString(R.string.add_card_text_error_bad_card_number));
            valid = false;
        }

        return valid;
    }

    //Mothod to replace the club card number from prefrences
    private void replaceCardNumber(String cardNumber) {
        userObj.setCardNumber(cardNumber);
        preferencesHelper.setString(PreferencesHelper.USER_OBJECT, gson.toJson(userObj));
    }

    //Method to close the actual sesion
    private void showAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                preferencesHelper.clearInfoByLogout();
                startActivity(new Intent(getActivity(), LoginActivity.class));
                getActivity().finish();
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });
        builder.setMessage(R.string.dialog_message)
                .setTitle(R.string.dialog_title);
        AlertDialog dialog = builder.create();
        dialog.show();
    }


}
