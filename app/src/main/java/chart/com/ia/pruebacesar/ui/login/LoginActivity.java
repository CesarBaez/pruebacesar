package chart.com.ia.pruebacesar.ui.login;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.google.gson.Gson;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import chart.com.ia.pruebacesar.App;
import chart.com.ia.pruebacesar.R;
import chart.com.ia.pruebacesar.Utils;
import chart.com.ia.pruebacesar.data.local.PreferencesHelper;
import chart.com.ia.pruebacesar.ui.home.HomeActivity;
import ia.com.commons.view.BaseActivity;

public class LoginActivity extends BaseActivity implements LoginPresenter.View {

    @Inject
    PreferencesHelper preferencesHelper;

    @Inject
    Context context;

    @Inject
    LoginPresenter presenter;

    @BindView(R.id.login_et_username)
    EditText etUserName;
    @BindView(R.id.login_et_password)
    EditText etPassword;

    private Gson gson;

    @BindView(R.id.login_btn_login)
    Button btnLogIn;

    private ProgressDialog progressDialog;

    @OnClick(R.id.login_btn_login)
    public void onClickSingIn() {
        btnLogIn.setEnabled(false);
        presenter.logIn(etUserName.getText().toString(), etPassword.getText().toString());

    }

    @Override
    protected void initActivity() {
        super.initActivity();
        App.getApplicationComponents().inject(this);
        gson = new Gson();
        hideLoading();
        if (preferencesHelper.getBoolean(PreferencesHelper.LOGGED,false)){
            showUser();
        }
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_login;
    }

    @Override
    protected void initPresenter() {
        super.initPresenter();
        presenter.setView(this);

    }

    @Override
    public void onErrorUserName(String error) {
        etUserName.setError(error);
        btnLogIn.setEnabled(true);
    }

    @Override
    public void onErrorPassword(String error) {
        etPassword.setError(error);
        btnLogIn.setEnabled(true);

    }


    @Override
    public void onLogInError(String error) {
        btnLogIn.setEnabled(true);
        Snackbar.make(this.getCurrentFocus(), error, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onSettedUserData(boolean status) {
        if (status){
          showUser();
        }
    }

    @Override
    public void showLoading() {
        progressDialog = ProgressDialog.show(this, null, getString(R.string.loading_data_text_dialog), true);
    }

    @Override
    public void hideLoading() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    private void showUser(){
        startActivity(new Intent(LoginActivity.this, HomeActivity.class));
        finish();
    }
}
