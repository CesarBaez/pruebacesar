package chart.com.ia.pruebacesar.ui.billboard;

import android.content.Context;

import com.google.gson.Gson;

import javax.inject.Inject;

import chart.com.ia.pruebacesar.data.DataConfiguration;
import chart.com.ia.pruebacesar.data.local.PreferencesHelper;
import chart.com.ia.pruebacesar.data.models.response.billboard.BillBoardResponse;
import chart.com.ia.pruebacesar.data.models.response.transactions.TransactionsResponse;
import chart.com.ia.pruebacesar.domain.BillBoardInteractor;
import ia.com.commons.view.BasePresenter;

/**
 * Created by fbaez on 21/02/2018.
 */

public class BillBoardPresenter extends BasePresenter<BillBoardPresenter.View> implements BillBoardInteractor.onBillBoardListener {

    @Inject
    PreferencesHelper preferencesHelper;
    @Inject
    Context context;
    @Inject
    Gson gson;

    private BillBoardInteractor billBoardInteractor;

    @Inject
    public BillBoardPresenter(BillBoardInteractor billBoardInteractor) {
        this.billBoardInteractor = billBoardInteractor;
        this.billBoardInteractor.setListener(this);
    }

    public void getBillBoard(String api_key) {
        billBoardInteractor.getBillBoard(api_key);
        if (isViewAttached()) {
            getView().showLoading();
        }
    }

    @Override
    public void onServiceError(String mensaje) {
        if (isViewAttached()) {
            getView().onBillBoardServiceError(mensaje);
            getView().hideLoading();
        }
    }

    @Override
    public void onDataSent(BillBoardResponse billBoardResponse) {
        if (isViewAttached()) {
            preferencesHelper.setString(PreferencesHelper.BILL_BOARD_OBJECT,gson.toJson(billBoardResponse));
            getView().onBillBoardServiceSuccess(billBoardResponse);
            getView().hideLoading();
        }

    }

    public interface View extends BasePresenter.View {
        void onBillBoardServiceSuccess(BillBoardResponse billBoardResponse);

        void onBillBoardServiceError(String error);

        @Override
        void showLoading();

        @Override
        void hideLoading();
    }
}
