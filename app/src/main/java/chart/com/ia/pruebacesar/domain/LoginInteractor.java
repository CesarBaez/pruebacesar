package chart.com.ia.pruebacesar.domain;

import android.content.Context;

import chart.com.ia.pruebacesar.data.models.request.login.LogInRequest;
import chart.com.ia.pruebacesar.data.models.response.login.LogInResponse;
import chart.com.ia.pruebacesar.data.models.response.user.UserResponse;
import chart.com.ia.pruebacesar.data.retrofit.services.LogInService;
import rx.Subscription;

/**
 * Created by fbaez on 20/02/2018.
 */

public class LoginInteractor {

    private OnLogin listener;
    private Context context;
    private Subscription subscription;
    private LogInService service;

    public LoginInteractor(Context context, LogInService service) {
        this.context = context;
        this.service = service;
    }

    public void logIn(LogInRequest request) {
        subscription = service.logIn("MX", request.getUserName(), request.getPassword(), "password", "IATestCandidate", "c840457e777b4fee9b510fbcd4985b68")
                .subscribe(logInResponse -> {
                    if (listener != null) {
                        listener.onLogIn(logInResponse);
                    }
                }, throwable -> {
                    if (listener != null) {
                        listener.onLogInError(throwable.getMessage());
                    }
                });
    }

    public void getUser(String api_key, String authorization) {
        subscription = service.getUser(api_key, authorization).subscribe(userResponse -> {
                    if (listener != null) {
                        listener.onUserDataSent(userResponse);
                    }
                }, throwable -> {
                    if (listener != null) {
                        listener.onLogInError(throwable.getMessage());
                    }
                }
        );
    }

    public void setListener(OnLogin listener) {
        this.listener = listener;
    }

    public interface OnLogin {

        void onLogInError(String mensaje);

        void onLogIn(LogInResponse logInResponse);

        void onUserDataSent(UserResponse userResponse);

    }
}
