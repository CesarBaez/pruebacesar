package chart.com.ia.pruebacesar.data.models.response.billboard;


import com.google.gson.annotations.SerializedName;


public class RoutesItem{

	@SerializedName("code")
	private String code;

	@SerializedName("sizes")
	private Sizes sizes;

	public void setCode(String code){
		this.code = code;
	}

	public String getCode(){
		return code;
	}

	public void setSizes(Sizes sizes){
		this.sizes = sizes;
	}

	public Sizes getSizes(){
		return sizes;
	}
}