package chart.com.ia.pruebacesar.data.models.response.complejos;


import com.google.gson.annotations.SerializedName;


public class ComplejoItem{

	@SerializedName("uris")
	private String uris;

	@SerializedName("settings")
	private Settings settings;

	@SerializedName("address")
	private String address;

	@SerializedName("lng")
	private String lng;

	@SerializedName("phone")
	private String phone;

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private int id;

	@SerializedName("vista_id")
	private String vistaId;

	@SerializedName("position")
	private int position;

	@SerializedName("lat")
	private String lat;

	@SerializedName("city_id")
	private int cityId;

	public void setUris(String uris){
		this.uris = uris;
	}

	public String getUris(){
		return uris;
	}

	public void setSettings(Settings settings){
		this.settings = settings;
	}

	public Settings getSettings(){
		return settings;
	}

	public void setAddress(String address){
		this.address = address;
	}

	public String getAddress(){
		return address;
	}

	public void setLng(String lng){
		this.lng = lng;
	}

	public String getLng(){
		return lng;
	}

	public void setPhone(String phone){
		this.phone = phone;
	}

	public String getPhone(){
		return phone;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setVistaId(String vistaId){
		this.vistaId = vistaId;
	}

	public String getVistaId(){
		return vistaId;
	}

	public void setPosition(int position){
		this.position = position;
	}

	public int getPosition(){
		return position;
	}

	public void setLat(String lat){
		this.lat = lat;
	}

	public String getLat(){
		return lat;
	}

	public void setCityId(int cityId){
		this.cityId = cityId;
	}

	public int getCityId(){
		return cityId;
	}
}