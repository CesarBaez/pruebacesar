package chart.com.ia.pruebacesar.ui.complejo;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import javax.inject.Inject;

import butterknife.BindView;
import chart.com.ia.pruebacesar.App;
import chart.com.ia.pruebacesar.R;
import chart.com.ia.pruebacesar.Utils;
import chart.com.ia.pruebacesar.data.local.PreferencesHelper;
import chart.com.ia.pruebacesar.data.models.response.complejos.ComplejoItem;
import ia.com.commons.view.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class ComplejoFragment extends BaseFragment implements ComplejoPresenter.View, OnMapReadyCallback {
    @Inject
    Context context;
    @Inject
    Gson gson;
    @Inject
    PreferencesHelper preferencesHelper;
    @Inject
    ComplejoPresenter presenter;

    private ProgressDialog progressDialog;

    @BindView(R.id.tv_complejo_phone_number)
    TextView tvPhoneNumber;
    @BindView(R.id.tv_complejo_name)
    TextView tvName;
    @BindView(R.id.tv_complejo_address)
    TextView tvAddress;

    private GoogleMap mMap;

    private ComplejoItem complejoObj;


    public ComplejoFragment() {
        // Required empty public constructor
    }


    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_complejo;
    }

    @Override
    protected void initFragment(@NonNull View view) {
        super.initFragment(view);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        initDagger();
        hideLoading();

    }

    private void initDagger() {
        App.getApplicationComponents().inject(this);
    }

    @Override
    protected void initPresenter() {
        super.initPresenter();
        presenter.setView(this);
        if (Utils.isMobileNetConected(context) || Utils.isWifiConected(context)) {
            presenter.getComplejo("199e2ce46ac525fddf");
        } else {
            Snackbar.make(getView(), getString(R.string.message_no_conexion), Snackbar.LENGTH_LONG).show();
            initWithLocalData();
        }

    }


    @Override
    public void onComplejoServiceSuccess(ComplejoItem complejoItem) {
        complejoObj = complejoItem;
        initDataViews();
    }

    @Override
    public void onComplejoServiceError(String error) {
        Snackbar.make(getView(), error, Snackbar.LENGTH_LONG).show();
        initWithLocalData();
    }

    @Override
    public void showLoading() {
        progressDialog = ProgressDialog.show(getActivity(), null, getString(R.string.loading_data_text_dialog), true);
    }

    @Override
    public void hideLoading() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    private void initDataViews() {
        tvName.setText(complejoObj.getName());
        tvAddress.setText(complejoObj.getAddress());
        tvPhoneNumber.setText(complejoObj.getPhone());
        if (mMap != null) {
            setCameraMapPosition(Double.parseDouble(complejoObj.getLng()), Double.parseDouble(complejoObj.getLat()), complejoObj.getName());
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (complejoObj != null) {
            setCameraMapPosition(Double.parseDouble(complejoObj.getLng()), Double.parseDouble(complejoObj.getLat()), complejoObj.getName());
        } else {
            setCameraMapPosition(-101.151, 19.687, getString(R.string.cinepolis));
        }

    }

    public void setCameraMapPosition(Double lon, Double lat, String complejo) {
        LatLng complejoLocation = new LatLng(lat, lon);
        mMap.clear();
        mMap.addMarker(new MarkerOptions().position(complejoLocation).title(complejo));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(complejoLocation, 17));
    }

    private void initWithLocalData() {
        if (!preferencesHelper.getString(PreferencesHelper.COMPLEJOS_OBJECT, "").isEmpty()) {
            complejoObj = gson.fromJson(preferencesHelper.getString(PreferencesHelper.COMPLEJOS_OBJECT, ""), ComplejoItem.class);
            initDataViews();
        }
    }
}
