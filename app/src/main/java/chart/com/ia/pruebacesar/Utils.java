package chart.com.ia.pruebacesar;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.widget.ImageView;

import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.ShareMediaContent;
import com.facebook.share.model.ShareOpenGraphAction;
import com.facebook.share.model.ShareOpenGraphContent;
import com.facebook.share.model.ShareOpenGraphObject;
import com.facebook.share.model.ShareVideo;
import com.facebook.share.model.ShareVideoContent;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;

import chart.com.ia.pruebacesar.R;

/**
 * Created by fbaez on 20/02/2018.
 */

public class Utils {

    public static void putImageOnView(ImageView imageView, String url, Context context) {
        Picasso.with(context)
                .load(String.format("%s", url))
                .placeholder(R.drawable.boletos_cartel_generico)
                .into(imageView);

    }

    public static ShareLinkContent getSharedLinkContent(String url, String title) {
        return new ShareLinkContent.Builder()
                .setContentUrl(Uri.parse(url))
                .setQuote(title)
                .build();
    }


    public static void showAlertDialog(String mensage, String title, Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });
        builder.setMessage(mensage)
                .setTitle(title);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public static String hourFormater(String dateTime) {

        final String time = dateTime.substring(dateTime.indexOf("T") + 1, dateTime.length());
        try {
            SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm:ss");
            SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
            Date _24HourDt = _24HourSDF.parse(time);
            return (_12HourSDF.format(_24HourDt)).toUpperCase();
        } catch (Exception e) {
            e.printStackTrace();
            return " ";
        }
    }

    public static String dateFormater(Context context, String dateTime) {
        final String date = dateTime.substring(0, dateTime.indexOf("T"));
        String AuxDate = "";
        String months[] = context.getResources().getStringArray(R.array.mes);
        SimpleDateFormat dayFormat = new SimpleDateFormat("dd");
        SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");

        try {
            SimpleDateFormat baseDateSDF = new SimpleDateFormat("yyyy-MM-dd");
            Date basicDate = baseDateSDF.parse(date);
            AuxDate = AuxDate + dayFormat.format(basicDate) + ", "
                    + months[basicDate.getMonth()] + " "
                    + yearFormat.format(basicDate);
            return AuxDate;
        } catch (Exception e) {
            e.printStackTrace();
            return " ";
        }
    }

    public static Boolean isWifiConected(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (info != null) {
                if (info.isConnected()) {
                    return true;
                }
            }
        }
        return false;
    }

    public static Boolean isMobileNetConected(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            if (info != null) {
                if (info.isConnected()) {
                    return true;
                }
            }
        }
        return false;
    }


}
