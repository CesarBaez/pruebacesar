package chart.com.ia.pruebacesar.data.models.response.billboard;

import java.util.List;
import com.google.gson.annotations.SerializedName;


public class CastItem{

	@SerializedName("label")
	private String label;

	@SerializedName("value")
	private List<String> value;

	public void setLabel(String label){
		this.label = label;
	}

	public String getLabel(){
		return label;
	}

	public void setValue(List<String> value){
		this.value = value;
	}

	public List<String> getValue(){
		return value;
	}
}