package chart.com.ia.pruebacesar.data.models.response.billboard;


import com.google.gson.annotations.SerializedName;


public class MediaItem{

	@SerializedName("code")
	private String code;

	@SerializedName("resource")
	private String resource;

	@SerializedName("type")
	private String type;

	public void setCode(String code){
		this.code = code;
	}

	public String getCode(){
		return code;
	}

	public void setResource(String resource){
		this.resource = resource;
	}

	public String getResource(){
		return resource;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}
}