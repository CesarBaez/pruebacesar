package chart.com.ia.pruebacesar.data.retrofit.services;

import chart.com.ia.pruebacesar.data.models.response.billboard.BillBoardResponse;
import retrofit2.http.Header;
import rx.Observable;

/**
 * Created by fbaez on 21/02/2018.
 */

public interface BillBoardService {
    Observable<BillBoardResponse> getBillBoard(@Header("api_key") String api_key);
}
