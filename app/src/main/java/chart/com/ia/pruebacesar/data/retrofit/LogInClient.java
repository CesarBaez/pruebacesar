package chart.com.ia.pruebacesar.data.retrofit;

import chart.com.ia.pruebacesar.data.models.response.login.LogInResponse;
import chart.com.ia.pruebacesar.data.models.response.user.UserResponse;
import chart.com.ia.pruebacesar.data.retrofit.services.LogInRetrofitService;
import chart.com.ia.pruebacesar.data.retrofit.services.LogInService;
import rx.Observable;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by fbaez on 20/02/2018.
 */

public class LogInClient implements LogInService {

    private LogInRetrofitService service;

    public LogInClient(LogInRetrofitService service) {
        this.service = service;
    }


    @Override
    public Observable<LogInResponse> logIn(String country_code, String username, String password, String grant_type, String client_id, String client_secret) {
        return service.logIn(country_code, username, password, grant_type, client_id, client_secret)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<UserResponse> getUser(String api_key, String Authorization) {
        return service.getUser(api_key, Authorization)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
