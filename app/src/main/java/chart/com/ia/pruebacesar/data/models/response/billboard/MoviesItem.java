package chart.com.ia.pruebacesar.data.models.response.billboard;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class MoviesItem{

	@SerializedName("code")
	private String code;

	@SerializedName("rating")
	private String rating;

	@SerializedName("length")
	private String length;

	@SerializedName("media")
	private List<MediaItem> media;

	@SerializedName("synopsis")
	private String synopsis;

	@SerializedName("cast")
	private List<CastItem> cast;

	@SerializedName("cinemas")
	private List<Integer> cinemas;

	@SerializedName("release_date")
	private String releaseDate;

	@SerializedName("original_name")
	private String originalName;

	@SerializedName("genre")
	private String genre;

	@SerializedName("name")
	private String name;

	@SerializedName("position")
	private int position;

	@SerializedName("categories")
	private List<String> categories;

	@SerializedName("id")
	private int id;

	public void setCode(String code){
		this.code = code;
	}

	public String getCode(){
		return code;
	}

	public void setRating(String rating){
		this.rating = rating;
	}

	public String getRating(){
		return rating;
	}

	public void setLength(String length){
		this.length = length;
	}

	public String getLength(){
		return length;
	}

	public void setMedia(List<MediaItem> media){
		this.media = media;
	}

	public List<MediaItem> getMedia(){
		return media;
	}

	public void setSynopsis(String synopsis){
		this.synopsis = synopsis;
	}

	public String getSynopsis(){
		return synopsis;
	}

	public void setCast(List<CastItem> cast){
		this.cast = cast;
	}

	public List<CastItem> getCast(){
		return cast;
	}

	public void setCinemas(List<Integer> cinemas){
		this.cinemas = cinemas;
	}

	public List<Integer> getCinemas(){
		return cinemas;
	}

	public void setReleaseDate(String releaseDate){
		this.releaseDate = releaseDate;
	}

	public String getReleaseDate(){
		return releaseDate;
	}

	public void setOriginalName(String originalName){
		this.originalName = originalName;
	}

	public String getOriginalName(){
		return originalName;
	}

	public void setGenre(String genre){
		this.genre = genre;
	}

	public String getGenre(){
		return genre;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setPosition(int position){
		this.position = position;
	}

	public int getPosition(){
		return position;
	}

	public void setCategories(List<String> categories){
		this.categories = categories;
	}

	public List<String> getCategories(){
		return categories;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}
}