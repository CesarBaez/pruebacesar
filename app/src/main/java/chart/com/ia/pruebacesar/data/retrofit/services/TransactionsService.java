package chart.com.ia.pruebacesar.data.retrofit.services;



import chart.com.ia.pruebacesar.data.models.request.transactions.TransactionsRequest;
import chart.com.ia.pruebacesar.data.models.response.transactions.TransactionsResponse;
import retrofit2.http.Body;
import retrofit2.http.Header;
import rx.Observable;

/**
 * Created by fbaez on 20/02/2018.
 */

public interface TransactionsService {
    Observable<TransactionsResponse> getTransactions(@Header("Authorization") String authorization,
                                                     @Body TransactionsRequest transactionsRequest);
}
