package chart.com.ia.pruebacesar.data.retrofit;

import chart.com.ia.pruebacesar.data.models.response.complejos.ComplejoItem;
import chart.com.ia.pruebacesar.data.retrofit.services.ComplejosRetrofitService;
import chart.com.ia.pruebacesar.data.retrofit.services.ComplejosService;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by fbaez on 21/02/2018.
 */

public class ComplejosClient implements ComplejosService {
    private ComplejosRetrofitService service;

    public ComplejosClient(ComplejosRetrofitService service) {
        this.service = service;
    }

    @Override
    public Observable<ComplejoItem> getComplejo(String api_key) {
        return service.getComplejo(api_key)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
