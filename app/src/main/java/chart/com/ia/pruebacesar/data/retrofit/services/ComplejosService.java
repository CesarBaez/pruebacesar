package chart.com.ia.pruebacesar.data.retrofit.services;

import chart.com.ia.pruebacesar.data.models.response.complejos.ComplejoItem;
import retrofit2.http.Header;
import rx.Observable;

/**
 * Created by fbaez on 21/02/2018.
 */

public interface ComplejosService {
    Observable<ComplejoItem> getComplejo(@Header("api_key") String api_key);
}
