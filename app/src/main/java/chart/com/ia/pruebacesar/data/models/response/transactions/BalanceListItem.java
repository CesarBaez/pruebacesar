package chart.com.ia.pruebacesar.data.models.response.transactions;


import com.google.gson.annotations.SerializedName;


public class BalanceListItem{

	@SerializedName("balance")
	private double balance;

	@SerializedName("name")
	private String name;

	@SerializedName("message")
	private String message;

	@SerializedName("key")
	private String key;

	public void setBalance(double balance){
		this.balance = balance;
	}

	public double getBalance(){
		return balance;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setKey(String key){
		this.key = key;
	}

	public String getKey(){
		return key;
	}
}