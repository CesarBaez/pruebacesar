package chart.com.ia.pruebacesar.data.retrofit;

import chart.com.ia.pruebacesar.data.models.request.transactions.TransactionsRequest;
import chart.com.ia.pruebacesar.data.models.response.transactions.TransactionsResponse;
import chart.com.ia.pruebacesar.data.retrofit.services.TransactionsRetrofitService;
import chart.com.ia.pruebacesar.data.retrofit.services.TransactionsService;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by fbaez on 20/02/2018.
 */

public class TransactionsClient implements TransactionsService {
    private TransactionsRetrofitService service;

    public TransactionsClient(TransactionsRetrofitService service) {
        this.service = service;
    }

    @Override
    public Observable<TransactionsResponse> getTransactions(String authorization, TransactionsRequest transactionsRequest) {
        return service.getTransactions(authorization, transactionsRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
