package chart.com.ia.pruebacesar.data.models.response.transactions;


import com.google.gson.annotations.SerializedName;


public class Level{

	@SerializedName("next_level")
	private String nextLevel;

	@SerializedName("advance_percent")
	private Double advancePercent;

	@SerializedName("name")
	private String name;

	@SerializedName("message")
	private String message;

	@SerializedName("key")
	private String key;

	public void setNextLevel(String nextLevel){
		this.nextLevel = nextLevel;
	}

	public String getNextLevel(){
		return nextLevel;
	}

	public void setAdvancePercent(Double advancePercent){
		this.advancePercent = advancePercent;
	}

	public Double getAdvancePercent(){
		return advancePercent;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setKey(String key){
		this.key = key;
	}

	public String getKey(){
		return key;
	}
}