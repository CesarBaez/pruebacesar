package chart.com.ia.pruebacesar.ui.login;

import android.content.Context;

import com.google.gson.Gson;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import chart.com.ia.pruebacesar.R;
import chart.com.ia.pruebacesar.data.local.PreferencesHelper;
import chart.com.ia.pruebacesar.data.models.request.login.LogInRequest;
import chart.com.ia.pruebacesar.data.models.response.login.LogInResponse;
import chart.com.ia.pruebacesar.data.models.response.user.UserResponse;
import chart.com.ia.pruebacesar.domain.LoginInteractor;
import ia.com.commons.view.BasePresenter;

/**
 * Created by fbaez on 20/02/2018.
 */

public class LoginPresenter extends BasePresenter<LoginPresenter.View> implements LoginInteractor.OnLogin{
    private LoginInteractor interactor;
    private Gson gson;

    @Inject
    PreferencesHelper preferencesHelper;
    @Inject
    Context context;

    @Inject
    public LoginPresenter(LoginInteractor interactor) {
        this.interactor = interactor;
        this.interactor.setListener(this);
        gson=new Gson();
    }

    public void logIn(String email, String password) {
        if (validLogin(email, password)) {
            LogInRequest request = new LogInRequest();
            request.setUserName(email);
            request.setPassword(password);
            interactor.logIn(request);
        }
        if (isViewAttached()) {
            getView().showLoading();
        }
    }

    public void getUserData(String api_key, String authorization){
    interactor.getUser(api_key,authorization);
    }

    @Override
    public void onLogInError(String mensaje) {
        if (isViewAttached()) {
            getView().hideLoading();
            getView().onLogInError(mensaje);
        }

    }

    @Override
    public void onLogIn(LogInResponse logInResponse) {
        preferencesHelper.setString(PreferencesHelper.TOKEN_OBJECT,gson.toJson(logInResponse));
        getUserData("199e2ce46ac525fddf","bearer "+logInResponse.getAccessToken());
    }

    @Override
    public void onUserDataSent(UserResponse userResponse) {
        preferencesHelper.setString(PreferencesHelper.USER_OBJECT,gson.toJson(userResponse));
        preferencesHelper.setBoolean(PreferencesHelper.LOGGED,true);
        if (isViewAttached()){
            getView().hideLoading();
            getView().onSettedUserData(true);
        }

    }

    //Metodo para validar las credenciales de login
    public boolean validLogin(String email, String password) {
        boolean valid = true;
        if (email.isEmpty()) {
            if (isViewAttached()) {
                getView().onErrorUserName(context.getString(R.string.login_text_error_is_empty_email));
            }
            valid = false;

        }
        if (password.isEmpty()) {
            if (isViewAttached()) {
                getView().onErrorPassword(context.getString(R.string.login_text_error_is_empty_password));
            }
            valid = false;
        }

        if (!isEmailValid(email)){
            if (isViewAttached()){
                getView().onErrorUserName(context.getString(R.string.login_text_error_is_not_email));
            }
            valid=false;
        }

        return valid;
    }


    //Metodo para validar que el usuario introducido se un email valido
    public static boolean isEmailValid(String email) {
        boolean isValid = false;
       //TODO borrar esta linea Pattern pattern = Pattern.compile(".+@.+.[a-z]+", Pattern.CASE_INSENSITIVE);
        Matcher matcher = Pattern.compile(".+@.+.[a-z]+", Pattern.CASE_INSENSITIVE).matcher(email);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    public interface View extends BasePresenter.View {
        void onErrorUserName(String error);

        void onErrorPassword(String error);

        void onLogInError(String error);

        void onSettedUserData(boolean status);

        @Override
        void showLoading();

        @Override
        void hideLoading();
    }
}
