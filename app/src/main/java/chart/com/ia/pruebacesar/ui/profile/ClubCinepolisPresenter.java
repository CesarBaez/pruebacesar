package chart.com.ia.pruebacesar.ui.profile;

import android.content.Context;

import com.google.gson.Gson;

import javax.inject.Inject;

import chart.com.ia.pruebacesar.data.local.PreferencesHelper;
import chart.com.ia.pruebacesar.data.models.request.transactions.TransactionsRequest;
import chart.com.ia.pruebacesar.data.models.response.transactions.TransactionsResponse;
import chart.com.ia.pruebacesar.domain.TransactionsInteractor;
import ia.com.commons.view.BasePresenter;

/**
 * Created by fbaez on 20/02/2018.
 */

public class ClubCinepolisPresenter extends BasePresenter<ClubCinepolisPresenter.View> implements TransactionsInteractor.OnTransactionsListener {
    @Inject
    PreferencesHelper preferencesHelper;
    @Inject
    Context context;
    @Inject
    Gson gson;

    private TransactionsInteractor interactor;

    @Inject
    public ClubCinepolisPresenter(TransactionsInteractor interactor) {
        this.interactor = interactor;
        this.interactor.setListener(this);

    }

    public void getTransactions(String authorization, String carNumber) {
        TransactionsRequest request = new TransactionsRequest();
        request.setCardNumber(carNumber);
        request.setCountryCode("MX");
        request.setTransactionInclude(true);
        interactor.getTransactions(authorization, request);
        if (isViewAttached()) {
            getView().showLoading();
        }
    }

    @Override
    public void onServiceError(String mensaje) {
        if (isViewAttached()) {
            getView().onTransactionServiceError(mensaje);
            getView().hideLoading();
        }
    }

    @Override
    public void onDataSent(TransactionsResponse transactionsResponse) {
        if (isViewAttached()) {
            preferencesHelper.setString(PreferencesHelper.TRANSACTIONS_OBJECT, gson.toJson(transactionsResponse));
            getView().onTransactionsServiceSuccess(transactionsResponse);
            getView().hideLoading();
        }

    }

    public interface View extends BasePresenter.View {
        void onTransactionsServiceSuccess(TransactionsResponse transactionsResponse);

        void onTransactionServiceError(String error);

        @Override
        void showLoading();

        @Override
        void hideLoading();
    }
}
