package chart.com.ia.pruebacesar;

import android.support.multidex.MultiDexApplication;

import chart.com.ia.pruebacesar.data.DataConfiguration;
import chart.com.ia.pruebacesar.di.data.DataModule;
import chart.com.ia.pruebacesar.di.main.components.ApplicationComponent;
import chart.com.ia.pruebacesar.di.main.components.DaggerApplicationComponent;
import chart.com.ia.pruebacesar.di.main.modules.ApplicationModule;

/**
 * Created by fbaez on 20/02/2018.
 */

public class App extends MultiDexApplication {

    private static ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initDagger();
    }

    private void initDagger() {
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .dataModule(new DataModule(DataConfiguration.BASE_URL))
                .build();
    }

    public static ApplicationComponent getApplicationComponents() {
        return applicationComponent;
    }
}
