package chart.com.ia.pruebacesar.data.models.request.transactions;


import com.google.gson.annotations.SerializedName;


public class TransactionsRequest{

	@SerializedName("country_code")
	private String countryCode;

	@SerializedName("card_number")
	private String cardNumber;

	@SerializedName("transaction_include")
	private boolean transactionInclude;

	public void setCountryCode(String countryCode){
		this.countryCode = countryCode;
	}

	public String getCountryCode(){
		return countryCode;
	}

	public void setCardNumber(String cardNumber){
		this.cardNumber = cardNumber;
	}

	public String getCardNumber(){
		return cardNumber;
	}

	public void setTransactionInclude(boolean transactionInclude){
		this.transactionInclude = transactionInclude;
	}

	public boolean isTransactionInclude(){
		return transactionInclude;
	}
}