package chart.com.ia.pruebacesar.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import chart.com.ia.pruebacesar.R;
import chart.com.ia.pruebacesar.Utils;
import chart.com.ia.pruebacesar.data.models.response.transactions.TransactionsItem;

/**
 * Created by fbaez on 21/02/2018.
 */

public class TransactionsAdapter extends RecyclerView.Adapter<TransactionsAdapter.ViewHolder> {

    private List<TransactionsItem> list;
    private Context context;

    public TransactionsAdapter(List<TransactionsItem> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.club_cinepolis_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        TransactionsItem item = list.get(position);
        holder.tvDate.setText(Utils.dateFormater(context,item.getDate())+" "+Utils.hourFormater(item.getDate()));
        holder.tvConcepto.setText(item.getMessage());
        holder.tvComplejo.setText(item.getCinema());
        holder.tvPuntos.setText(String.valueOf(item.getPoints()));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvDate;
        TextView tvConcepto;
        TextView tvComplejo;
        TextView tvPuntos;

        public ViewHolder(View itemView) {
            super(itemView);
            tvPuntos = (TextView) itemView.findViewById(R.id.tv_transaction_puntos);
            tvComplejo = (TextView) itemView.findViewById(R.id.tv_transaction_complejo);
            tvConcepto = (TextView) itemView.findViewById(R.id.tv_transaction_concepto);
            tvDate = (TextView) itemView.findViewById(R.id.tv_transaction_date);
        }
    }
}
