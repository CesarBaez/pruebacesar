package chart.com.ia.pruebacesar.domain;

import android.content.Context;

import chart.com.ia.pruebacesar.data.models.response.complejos.ComplejoItem;
import chart.com.ia.pruebacesar.data.retrofit.services.ComplejosService;
import rx.Subscription;

/**
 * Created by fbaez on 21/02/2018.
 */

public class ComplejoInteractor {

    private ComplejosService service;
    private Context context;
    private Subscription subscription;
    private onComplejosListener listener;

    public ComplejoInteractor(ComplejosService service, Context context) {
        this.service = service;
        this.context = context;
    }

    public void getComplejo(String api_key) {
        subscription = service.getComplejo(api_key).subscribe(complejoItem -> {
            if (listener != null) {
                listener.onDataSent(complejoItem);
            }
        }, throwable -> {
            if (listener != null) {
                listener.onServiceError(throwable.getMessage());
            }
        });
    }

    public void setListener(onComplejosListener listener) {
        this.listener = listener;
    }

    public interface onComplejosListener {
        void onServiceError(String mensaje);

        void onDataSent(ComplejoItem complejoItem);
    }
}
